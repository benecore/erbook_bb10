/*
 * cacher.h
 *
 *  Created on: 20.1.2014
 *      Author: Benecore
 */

#ifndef CACHER_H_
#define CACHER_H_

#include <qobject.h>
#include <QFile>
#include <QDir>
#include <QDebug>
#include <bb/data/JsonDataAccess>

using namespace bb::data;

class Cacher: public QObject
{
	Q_OBJECT
public:
	Cacher(QObject *parent = 0);
	virtual ~Cacher();

	static Cacher *instance();

public slots:
	void saveLocal(const QByteArray &data);
	void updateLocal(const QByteArray &data);
	void removeLocal(const QVariant &data);
	QByteArray getLocal();
	void saveTags(const QByteArray &data);
	QByteArray getTags();
	QVariantList getBookmarkTags(const QVariant &bookmarkId);
	void removeTag(const QVariant &id);
	void removeBookmarkTag(const QVariant &bookmarkId, const QVariant &tagId);
	int getTotalArticles();
	void saveRemote(const QByteArray &data);
	QByteArray getRemote();
	QList<QVariant> compare();

	void addTags(const QString &id, const QVariantList &tags);
	void saveUserInfo(const QByteArray &data);
	QVariant getUserInfo();


private:
	Q_DISABLE_COPY(Cacher)
	static Cacher *_instance;
	QFile *_local,
	*_remote,
	*_user,
	*_tags;
};

#endif /* CACHER_H_ */
