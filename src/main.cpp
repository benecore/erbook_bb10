#include <bb/cascades/Application>
#include <bb/ApplicationInfo>
#include <Flurry.h>
#include <QLocale>
#include <QTranslator>
#include "applicationui.hpp"
#include <Qt/qdeclarativedebug.h>

using namespace bb::cascades;


void myMessageOutput(QtMsgType type, const char* msg){
	Q_UNUSED(type)
    fprintf(stdout, "%s\n", msg);
    fflush(stdout);
}


QByteArray getTheme(){
	return ThemeSettings::instance()->getThemeString("theme", "bright").toUtf8();
}


Q_DECL_EXPORT int main(int argc, char **argv)
{
#if !defined(QT_NO_DEBUG)
	Flurry::Analytics::SetDebugLogEnabled(true);
#endif

	// Read and log the version of the app being used
	Flurry::Analytics::SetAppVersion(bb::ApplicationInfo().version());

	//Flurry::Analytics::StartSession("P793BVW6YVV3SVVZW65K");


	qputenv("CASCADES_THEME", getTheme());
    Application app(argc, argv);
    app.setOrganizationName("DevPDA");
    app.setApplicationName("eRBook");
    qInstallMsgHandler(myMessageOutput);

    // Create the Application UI object, this is where the main.qml file
    // is loaded and the application scene is set.
    new ApplicationUI(&app);

    // Enter the application main event loop.
    return Application::exec();
}
