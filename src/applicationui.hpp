/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/ThemeSupport>
#include <bb/cascades/Theme>
#include <bb/cascades/ColorTheme>
#include <bb/cascades/Label>
#include <bb/cascades/Container>
#include <bb/cascades/SceneCover>


/** Platform features **/
#include "platform/InviteToDownload.hpp"
#include "platform/RegistrationHandler.hpp"
#include "platform/StatusEventHandler.h"


/** Custom classes **/
#include "custom/sizehelper.h"
#include "custom/webimageview.h"
#include "custom/timer.h"
#include "settings/themesettings.h"
#include "custom/networkmonitor.h"
#include "custom/invoker.h"
#include "ui/syncdialog.h"

/** Database **/
#include "db/database.h"

/** Settings **/
#include "settings/settings.h"

/** Live coding **/
#include "livecoding/qmlbeam.h"


/** Readability Library **/
#include <QtReadability>


#include "cacher.h"
#include "helper.h"
#include <bb/data/JsonDataAccess>
#include <bb/device/HardwareInfo>


/** Invoke **/
#include <bb/system/CardDoneMessage>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeRequest>
#include <bb/cascades/Invocation>
#include <bb/cascades/Invocation>
#include <bb/cascades/InvokeQuery>


using namespace bb::cascades;
using namespace bb::platform;
using namespace bb::data;
using namespace bb::system;

namespace bb
{
namespace cascades
{
class Application;
class LocaleHandler;
class Invocation;
}
}

class QTranslator;

/*!
 * @brief Application object
 *
 *
 */

class ApplicationUI : public QObject
{
	Q_OBJECT
	/** Connection online/type **/
	Q_PROPERTY(bool online READ online NOTIFY onlineChanged)
	Q_PROPERTY(QString netType READ netType NOTIFY onlineChanged)
	/** Model **/
	Q_PROPERTY(bb::cascades::GroupDataModel* model READ model CONSTANT)
	Q_PROPERTY(bb::cascades::GroupDataModel* tagsModel READ tagsModel CONSTANT)
	Q_PROPERTY(int modelSize READ modelSize NOTIFY modelSizeChanged)
	Q_PROPERTY(int tagsSize READ tagsSize NOTIFY tagsModelChanged)
	Q_PROPERTY(QVariantList indexPath READ indexPath WRITE setIndexPath NOTIFY indexPathChanged)
	/** Is offline data ? **/
	Q_PROPERTY(bool isOffline READ isOffline CONSTANT)
	Q_PROPERTY(bool loading READ loading WRITE setLoading NOTIFY loadingChanged)
	Q_PROPERTY(bool syncing READ syncing WRITE setSyncing NOTIFY syncingChanged)
	Q_PROPERTY(bool updating READ updating WRITE setUpdating NOTIFY updatingChanged)
	/** White theme ? **/
	Q_PROPERTY(bool whiteTheme READ whiteTheme CONSTANT)

	/** Invocation **/
	// The textual representation of the startup mode of the application
	Q_PROPERTY(QString startupMode READ startupMode NOTIFY requestChanged)

	// The values of the incoming invocation request
	Q_PROPERTY(QString source READ source NOTIFY requestChanged)
	Q_PROPERTY(QString target READ target NOTIFY requestChanged)
	Q_PROPERTY(QString action READ action NOTIFY requestChanged)
	Q_PROPERTY(QString mimeType READ mimeType NOTIFY requestChanged)
	Q_PROPERTY(QString uri READ uri NOTIFY requestChanged)
	Q_PROPERTY(QString data READ data NOTIFY requestChanged)

	// The textual representation of the card status
	Q_PROPERTY(QString status READ status NOTIFY statusChanged)
public:
	ApplicationUI(Application *app);
	virtual ~ApplicationUI();


	Q_INVOKABLE
	void setSortingKeys(const QString &sortingKey);
	Q_INVOKABLE
	void setSortAscending(bool ascending);
	Q_INVOKABLE
	void setGrouping(const QString &grouping = "none");

	Q_INVOKABLE
	void setTagsSortingKeys(const QStringList &sortingKeys);
	Q_INVOKABLE
	void setTagsSortAscending(bool ascending);
	Q_INVOKABLE
	void setTagsGrouping(const QString &grouping = "none");

	Q_INVOKABLE
	void accessToken(const QUrl &url);
	Q_INVOKABLE
	void login(const QString &username, const QString &password);
	Q_INVOKABLE
	void bookmarks(const bool &refresh = false, const QString &page = "1", const QString &per_page = "20"); // per_page = max 50 default 20 items
	Q_INVOKABLE
	void addBookmark(const QString &url, const int &favorite, const int &archive, const int &allow_duplicates);
	Q_INVOKABLE
	void updateBookmark(const QString &bookmarkId, const int &favorite, const int &archive, const float &read_percent = 0.0);
	Q_INVOKABLE
	void removeBookmark(const QString &bookmarkId);
	Q_INVOKABLE
	void article(const QString &bookmarkId);
	Q_INVOKABLE
	void userInfo();
	Q_INVOKABLE
	void getShortUrl(const QString &sourceUrl);
	Q_INVOKABLE
	void getTags();
	Q_INVOKABLE
	void addTagsToBookmark(const QString &bookmarkId, const QString &tags);
	Q_INVOKABLE
	void deleteTag(const QString &tagId);
	Q_INVOKABLE
	void deleteBookmarkTag(const QString &bookmarkId, const QString &tagId);
	Q_INVOKABLE
	void deleteSelectedTags(const QVariantList selectedTags);
	Q_INVOKABLE
	void cardDone(const QString& msg);

	Q_INVOKABLE
	void share(QString thedata);



	Invocation *m_pInvocation;


	/** Filtering Model **/
	Q_INVOKABLE
	void showReading();
	Q_INVOKABLE
	void showFavorites();
	Q_INVOKABLE
	void showArchive();
	Q_INVOKABLE
	void showTags();
	Q_INVOKABLE
	void showTagsBookmark(const QVariantList &bookmarksId = QVariantList());

	/** Sync content **/
	Q_INVOKABLE
	void syncContent();

	signals:
	void invokationMessage(const QString message);
	void onlineChanged(bool online, QString type);
	void loadingChanged(bool loading);
	void syncingChanged(bool syncing);
	void updatingChanged(bool updating);
	void modelSizeChanged(int _modelSize);
	void tagsModelChanged(int _tagsSize);
	void indexPathChanged(QVariantList indexPath);
	void loginDone();
	void currentSyncing(const int current, const int all);
	void syncingDone();
	void bookmarksDone();
	void updateBookmarkDone(const QVariantMap bookmark);
	void articleContentDone(const QVariant content);
	void userInfoDone(const QVariantMap user);
	/** Invocation **/
	void requestChanged();
	void statusChanged();
	void authorizationUrlDone(QUrl authorizationUrl);
	void invokeShareUrl(const QString shortUrl);

	void tagsDone(QByteArray response);
	void bookmarkTagDeleted();

	private slots:
	void initFullUI();
	void initCardUI();
	void onArmed();

	void canceledSync();

	inline bool online() const { return _online; }
	inline QString netType() const { return _netType; }
	void networkStatusUpdated(bool status, QString type);
	void onSystemLanguageChanged();
	inline bool whiteTheme() const { return Application::instance()->themeSupport()->theme()->colorTheme()->style() == VisualStyle::Bright; }
	inline GroupDataModel *model() const { return _model; };
	inline GroupDataModel *tagsModel() const { return _tagsModel; };
	inline int modelSize() const { return _model->size(); }
	inline int tagsSize() const { return _tagsModel->size(); }
	inline QVariantList indexPath() const { return _indexPath; }
	void setIndexPath(QVariantList &indexPath){
		if (_indexPath != indexPath){
			_indexPath = indexPath;
			emit indexPathChanged(_indexPath);
		}
	}
	inline bool isOffline() const { return _isOffline; }

	inline bool loading() const { return _loading; }
	inline void setLoading(bool loading){
		if (_loading != loading){
			_loading = loading;
			emit loadingChanged(_loading);
		}
	}
	inline bool syncing() const { return _syncing; }
	inline void setSyncing(bool syncing){
		if (_syncing != syncing){
			_syncing = syncing;
			emit syncingChanged(_syncing);
		}
	}
	inline bool updating() const { return _updating; }
	inline void setUpdating(bool updating){
		if (_updating != updating){
			_updating = updating;
			emit updatingChanged(_updating);
		}
	}

	void requestTokenReceived(QMap<QString, QString> response);
	void authorizationUrlReceived(QUrl authorizationUrl);
	void loginDone(QMap<QString, QString> tokens);

	void requestFinished(QByteArray response);
	void error(int error, QByteArray errorString);

	/** parsing **/
	void parseBookmarks(QByteArray response);
	void parseArticle(QByteArray response);
	void parseAddBookmark(QByteArray response);
	void parseUpdateBookmark(QByteArray response);
	void parseRemoveBookmark(QByteArray response);
	void parseUserInfo(QByteArray response);
	void parseShortUrl(QByteArray response);
	void parseGetTags(QByteArray response);
	void parseSetTags(QByteArray response);
	void parseDeleteTag(QByteArray response);
	void parseDeleteBookmarkTag(QByteArray response);

	private Q_SLOTS:
	// This slot is called whenever an invocation request is received
	void handleInvoke(const bb::system::InvokeRequest& request);

	void resized(const bb::system::CardResizeMessage& request);

	/**
	 * This slot is triggered when a card is chosen to be pooled by the system after it is closed. The card should clear it's UI
	 * and states so that it is ready for the next invocation.
	 */
	void pooled(const bb::system::CardDoneMessage& request);

	inline void internalServerError() const { _helper->showToast(tr("Internal server error. Try again later !")); }


	QString startupMode() const;
	QString source() const;
	QString target() const;
	QString action() const;
	QString mimeType() const;
	QString uri() const;
	QString data() const;
	QString status() const;



	private:
	QTranslator* m_pTranslator;
	LocaleHandler* m_pLocaleHandler;
	bool _loading,
	_isOffline,
	_syncing,
	_online,
	_updating;
	QString _netType;
	QVariantList articles;
	/** Platform **/
	RegistrationHandler *_registrationHandler;
	InviteToDownload *_inviteDownload;
	StatusEventHandler *_statusHandler;
	/** Custom **/
	SizeHelper *_sizeHelper;
	Database *_db;
	Settings *_settings;
	/** API **/
	QtReadability *_api;
	/** ROOT TabbedPane **/

	/** Global Model **/
	GroupDataModel *_model;
	GroupDataModel *_tagsModel;
	Cacher *_cacher;
	HelperUtils *_helper;
	QVariantList _indexPath;

	/** Invoking **/
	InvokeManager *_invokeManager;
	QString m_startupMode;
	QString m_source;
	QString m_target;
	QString m_action;
	QString m_mimeType;
	QString m_uri;
	QString m_data;
	QString m_status;
	bool _isInvokation;
	SyncDialog *syncDialog;
	bool syncCanceled;
	Invoker *_invoker;
	int _tagsSize;
	bool _fromAddPage;
};

#endif /* ApplicationUI_HPP_ */
