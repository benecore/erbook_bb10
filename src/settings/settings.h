/*
 * settings.h
 *
 *  Created on: 31.8.2013
 *      Author: Benecore
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <QObject>

#include <QSettings>
#include <bb/cascades/Application>

using namespace bb::cascades;

class Settings: public QObject
{
	Q_OBJECT
	/** PRO VERSION **/
	Q_PROPERTY(bool isPro READ isPro WRITE setIsPro NOTIFY isProChanged)
	Q_PROPERTY(bool listLayout READ listLayout WRITE setListLayout NOTIFY listLayoutChanged)
	/** Sorting **/
	Q_PROPERTY(bool sortAscending READ sortAscending WRITE setSortAscending NOTIFY sortAscendingChanged)
	Q_PROPERTY(QVariant sortingKeys READ sortingKeys WRITE setSortingKeys NOTIFY sortingKeysChanged)
	Q_PROPERTY(int activeTab READ activeTab WRITE setActiveTab NOTIFY activeTabChanged)
	Q_PROPERTY(bool syncStartup READ syncStartup WRITE setSyncStartup NOTIFY syncStartupChanged)
	Q_PROPERTY(bool loadImages READ loadImages WRITE setLoadImages NOTIFY loadImagesChanged)
	Q_PROPERTY(bool grouping READ grouping WRITE setGrouping NOTIFY groupingChanged)
	/** Colors **/
	Q_PROPERTY(QVariant activeColor READ activeColor WRITE setActiveColor NOTIFY activeColorChanged)
	/** font size **/
	Q_PROPERTY(int fontSize READ fontSize WRITE setFontSize NOTIFY fontSizeChanged)
	/** Reading settings **/
	Q_PROPERTY(QVariant backColor READ backColor WRITE setBackColor NOTIFY backColorChanged)
	Q_PROPERTY(int readMode READ readMode WRITE setReadMode NOTIFY styleSheetChanged)
	Q_PROPERTY(int readFamily READ readFamily WRITE setReadFamily NOTIFY styleSheetChanged)
	Q_PROPERTY(bool nightMode READ nightMode WRITE setNightMode NOTIFY nightModeChanged)
	Q_PROPERTY(bool fullScreen READ fullScreen WRITE setFullScreen NOTIFY fullScreenChanged)
	/** Shortener service **/
	Q_PROPERTY(bool useShortener READ useShortener WRITE setUseShortener NOTIFY useShortenerChanged)
	/* General settings */
	Q_PROPERTY(bool whiteHeader READ whiteHeader WRITE setWhiteHeader NOTIFY whiteHeaderChanged)
	Q_PROPERTY(bool syncAfterAdd READ syncAfterAdd WRITE setSyncAfterAdd NOTIFY syncAfterAddChanged)
	/** Tags settings **/
	Q_PROPERTY(bool tagsGrouping READ tagsGrouping WRITE setTagsGrouping NOTIFY tagsGroupingChanged)
	Q_PROPERTY(bool tagsSortAscending READ tagsSortAscending WRITE setTagsSortAscending NOTIFY tagsSortAscendingChanged)
	/** Invokation **/
	Q_PROPERTY(bool closeInvoke READ closeInvoke WRITE setCloseInvoke NOTIFY closeInvokeChanged)
public:
	explicit Settings(QObject *parent = 0);
	virtual ~Settings();

	static Settings *instance();
	static void destroy();


	signals:
	void isProChanged(bool isPro);
	void listLayoutChanged(bool listLayout);
	/** Sorting **/
	void sortAscendingChanged(bool sortAscending);
	void sortingKeysChanged(QVariant sortingKeys);
	void activeTabChanged(int activeTab);
	void syncStartupChanged(bool _syncStartup);
	void loadImagesChanged(bool _loadImages);
	void groupingChanged(bool grouping);
	void tagsGroupingChanged(bool tagsGrouping);
	void tagsSortAscendingChanged(bool tagsSortAscending);
	/** Colors **/
	void activeColorChanged(QVariant activeColor);
	/** font size **/
	void fontSizeChanged(int fontSize);

	/** Reading settings **/
	void backColorChanged(QVariant backColor);
	void nightModeChanged(bool nighMode);
	void fullScreenChanged(bool fullScreen);
	void styleSheetChanged(int readMode, int readFamily);
	void useShortenerChanged(bool useShortener);
	/* General settings */
	void whiteHeaderChanged(bool whiteHeader);
	void syncAfterAddChanged(bool syncAfterAdd);
	void closeInvokeChanged(bool closeInvoke);


	public slots:
	inline bool isPro() const { return _isPro; };
	inline void setIsPro(const bool &isPro){
		if (_isPro != isPro){
			_isPro = isPro;
			settings->setValue("isPro", _isPro);
			emit isProChanged(_isPro);
		}
	}
	inline bool listLayout() const { return _listLayout; };
	inline void setListLayout(const bool &value){
		if (_listLayout != value){
			_listLayout = value;
			settings->setValue("listLayout", _listLayout);
			emit listLayoutChanged(_listLayout);
		}
	}
	inline bool sortAscending() const { return _sortAscending; };
	inline void setSortAscending(const bool &value){
		if (_sortAscending != value){
			_sortAscending = value;
			settings->setValue("sortAscending", _sortAscending);
			emit sortAscendingChanged(_sortAscending);
		}
	}
	inline QVariant sortingKeys() const { return _sortingKeys; };
	inline void setSortingKeys(const QVariant &sortingKeys){
		if (_sortingKeys != sortingKeys){
			_sortingKeys = sortingKeys;
			settings->setValue("sortingKeys", _sortingKeys);
			emit sortingKeysChanged(_sortingKeys);
		}
	}
	inline int activeTab() const { return _activeTab; };
	inline void setActiveTab(const int &value){
		if (_activeTab != value){
			_activeTab = value;
			settings->setValue("activeTab", _activeTab);
			emit activeTabChanged(_activeTab);
		}
	}
	inline bool syncStartup() const { return _syncStartup; };
	inline void setSyncStartup(const bool &value){
		if (_syncStartup != value){
			_syncStartup = value;
			settings->setValue("syncStartup", _syncStartup);
			emit syncStartupChanged(_syncStartup);
		}
	}
	inline bool loadImages() const { return _loadImages; };
	inline void setLoadImages(const bool &value){
		if (_loadImages != value){
			_loadImages = value;
			settings->setValue("loadImages", _loadImages);
			emit loadImagesChanged(_loadImages);
		}
	}
	inline bool grouping() const { return _grouping; };
	inline void setGrouping(const bool &grouping){
		if (_grouping != grouping){
			_grouping = grouping;
			settings->setValue("grouping", _grouping);
			emit groupingChanged(_grouping);
		}
	}
	inline const QVariant& activeColor() const { return _activeColor; }
	inline void setActiveColor(QVariant &activeColor){
		if (_activeColor != activeColor){
			_activeColor = activeColor;
			settings->setValue("activeColor", _activeColor);
			emit activeColorChanged(_activeColor);
		}
	}
	inline const int& fontSize() const { return _fontSize; }
	inline void setFontSize(int &fontSize){
		if (_fontSize != fontSize){
			_fontSize = fontSize;
			settings->setValue("fontSize", _fontSize);
			emit fontSizeChanged(_fontSize);
		}
	}
	inline const QVariant& backColor() const { return _backColor; }
	inline void setBackColor(QVariant &backColor){
		if (_backColor != backColor){
			_backColor = backColor;
			settings->setValue("backColor", _backColor);
			emit backColorChanged(_backColor);
		}
	}
	inline const bool& nightMode() const { return _nightMode; }
	inline void setNightMode(bool &nightMode){
		if (_nightMode != nightMode){
			_nightMode = nightMode;
			settings->setValue("nightMode", _nightMode);
			emit nightModeChanged(_nightMode);
		}
	}
	inline const bool& fullScreen() const { return _fullScreen; }
	inline void setFullScreen(bool &fullScreen){
		if (_fullScreen != fullScreen){
			_fullScreen = fullScreen;
			settings->setValue("fullScreen", _fullScreen);
			emit fullScreenChanged(_fullScreen);
		}
	}
	/** Read mode **/
	inline const int& readMode() const { return _readMode; }
	inline void setReadMode(int &readMode){
		if (_readMode != readMode){
			_readMode = readMode;
			settings->setValue("readMode", _readMode);
			emit styleSheetChanged(_readMode, _readFamily);
		}
	}
	/** Font family **/
	inline const int& readFamily() const { return _readFamily; }
	inline void setReadFamily(int &readFamily){
		if (_readFamily != readFamily){
			_readFamily = readFamily;
			settings->setValue("readFamily", _readFamily);
			emit styleSheetChanged(_readMode, _readFamily);
		}
	}
	/** Shortener service **/
	inline const bool& useShortener() const { return _useShortener; }
	inline void setUseShortener(bool &useShortener){
		if (_useShortener != useShortener){
			_useShortener = useShortener;
			settings->setValue("useShortener", _useShortener);
			emit useShortenerChanged(_useShortener);
		}
	}
	/* General settings */
	inline const bool& whiteHeader() const { return _whiteHeader; }
	inline void setWhiteHeader(bool &whiteHeader){
		if (_whiteHeader != whiteHeader){
			_whiteHeader = whiteHeader;
			settings->setValue("whiteHeader", _whiteHeader);
			emit whiteHeaderChanged(_whiteHeader);
		}
	}
	inline const bool& syncAfterAdd() const { return _syncAfterAdd; }
	inline void setSyncAfterAdd(bool &syncAfterAdd){
		if (_syncAfterAdd != syncAfterAdd){
			_syncAfterAdd = syncAfterAdd;
			settings->setValue("syncAfterAdd", _syncAfterAdd);
			emit syncAfterAddChanged(_syncAfterAdd);
		}
	}
	/** Tags grouping **/
	inline bool tagsGrouping() const { return _tagsGrouping; };
	inline void setTagsGrouping(const bool &tagsGrouping){
		if (_tagsGrouping != tagsGrouping){
			_tagsGrouping = tagsGrouping;
			settings->setValue("tagsGrouping", _tagsGrouping);
			emit tagsGroupingChanged(_tagsGrouping);
		}
	}
	inline bool tagsSortAscending() const { return _tagsSortAscending; };
	inline void setTagsSortAscending(const bool &value){
		if (_tagsSortAscending != value){
			_tagsSortAscending = value;
			settings->setValue("tagsSortAscending", _tagsSortAscending);
			emit tagsSortAscendingChanged(_tagsSortAscending);
		}
	}
	/** Invokation **/
	inline bool closeInvoke() const { return _closeInvoke; };
	inline void setCloseInvoke(const bool &value){
		if (_closeInvoke != value){
			_closeInvoke = value;
			settings->setValue("closeInvoke", _closeInvoke);
			emit closeInvokeChanged(_closeInvoke);
		}
	}




	private:
	static Settings *_instance;
	QSettings *settings;
	QVariant _activeColor, _backColor, _sortingKeys;
	bool _isPro,
	_listLayout,
	_sortAscending,
	_syncStartup,
	_loadImages,
	_nightMode,
	_grouping,
	_fullScreen;
	int _activeTab,
	_fontSize;
	int _readMode;
	int _readFamily;
	bool _useShortener;
	bool _whiteHeader;
	bool _syncAfterAdd;
	bool _tagsGrouping;
	bool _tagsSortAscending;
	bool _closeInvoke;
};

#endif /* SETTINGS_H_ */
