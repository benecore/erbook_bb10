/*
 * database.h
 *
 *  Created on: 30.8.2013
 *      Author: Benecore
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include <QObject>
#include <QtSql>
#include <QList>

#include <QDebug>

class Database: public QObject
{
	Q_OBJECT
	Q_PROPERTY(bool isAuthorized READ isAuthorized NOTIFY authorizedChanged)
	Q_PROPERTY(QList<QVariant> authData READ authData NOTIFY authChanged)
public:
	explicit Database(QObject *parent = 0);
	virtual ~Database();

	static Database *instance();
	static void destroy();



signals:
	void authorizedChanged(bool);
	void authChanged(const QVariant token = "", const QVariant secret = "");
	void articlesChanged();
	void resetAuthDone();


public slots:
	bool isAuthorized() const;
	QList<QVariant> authData();
	void setAuth(const QVariant &token, const QVariant &secret);
	bool resetAuth();
	bool clearDatabase();

	void cache(const QVariant &id, const QVariant &content);
	bool isCached(const QVariant &id);
	bool clearCache();
	int cacheCount();
	QVariant getCache(const QVariant &id);
	bool removeCache(const QVariant &id);
	void updatePosition(const QVariant &id, const QVariant &position);
	float getPosition(const QVariant &id);
	bool shortUrlExists(const QVariant &id);
	bool setShortUrl(const QVariant &id, const QVariant &url);
	QVariant shortUrl(const QVariant &id);



private slots:
	void alert(const QString &message);
	void createTables();


private:
	static Database *_instance;
	QSqlDatabase *_db;

};

#endif /* DATABASE_H_ */
