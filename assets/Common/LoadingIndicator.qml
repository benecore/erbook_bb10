import bb.cascades 1.2


Container{
    id: root
    
    property alias text: label.text
    property alias loading: indicator.visible
    
    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }
    verticalAlignment: VerticalAlignment.Bottom
    horizontalAlignment: HorizontalAlignment.Fill
    topPadding: 30
    leftPadding: 30
    rightPadding: 30
    bottomPadding: 30
    Label {
        id: label
        objectName: "loadingIndicatorText"
        text: {
            if (App.loading){
                return qsTr("Loading articles...") + Retranslate.onLocaleOrLanguageChanged
            }
            else if (App.syncing){
                return qsTr("Downloading content... 0/0") + Retranslate.onLocaleOrLanguageChanged
            }
        }
        textStyle.fontSize: FontSize.Small
        textStyle.textAlign: TextAlign.Left
        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }                
    }
    ActivityIndicator {
        id: indicator
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Center
        running: loading
    }
    visible: indicator.visible
}