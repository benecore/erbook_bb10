import bb.cascades 1.2
import com.devpda.tools 1.2

Page {
    id: root
    
    property bool active: false
    property alias header: root.titleBar
    property alias rootLayout: rootContainer.layout // Default DockLayout
    property alias loading: loadingIndicator.visible
    property alias loadingText:loadingIndicator.text
    property alias noContentVisible: noContent.visible
    property alias noContentText: noContentLabel.text
    property alias noContentImageSource: noContentImage.imageSource
    property int noContentImageSize: 150
    default property alias contentItems: itemsContainer.controls
    property alias leftPadding: itemsContainer.leftPadding
    property alias rightPadding: itemsContainer.rightPadding
    property alias topPadding: itemsContainer.topPadding
    property alias bottomPadding: itemsContainer.bottomPadding
    property alias invoking: invoker
    
    attachedObjects: [
        Invoker{
            id: invoker
            onInvocationFailed: {
                console.log("Invokation failed")
            }
        }
    ]
    
    Container {
        id: rootContainer
        layout: DockLayout{}
        
        LoadingIndicator {
            id: loadingIndicator
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Bottom
        }
        
        
        Container {
            id: noContent
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            
            ImageView {
                id: noContentImage
                visible: (imageSource != "")
                preferredHeight: noContentImageSize
                preferredWidth: noContentImageSize
                scalingMethod: ScalingMethod.AspectFit
                horizontalAlignment: HorizontalAlignment.Center
                bottomMargin: 0
            }
            
            Label {
                id: noContentLabel
                visible: (text != "")
                multiline: true
                autoSize.maxLineCount: 2
                horizontalAlignment: HorizontalAlignment.Center
                textStyle{
                    base: SystemDefaults.TextStyles.BigText
                    color: Color.Gray
                }
                topMargin: 0
                textStyle.textAlign: TextAlign.Center
            }
        
        }
        
        Container {
            id: itemsContainer
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
        }
        
    }
}
