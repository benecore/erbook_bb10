import bb.cascades 1.2



Container {
    id: root
    
    
    property alias text: label.text
    property alias imageSource: image.imageSource
    
    layout: StackLayout {
    }
    
    
    
    ImageView {
        id: image
        horizontalAlignment: HorizontalAlignment.Center
    }
    
    
    Label {
        id: label
        horizontalAlignment: HorizontalAlignment.Center
        textStyle{
            base: SystemDefaults.TextStyles.BigText
            color: Color.Gray
        }
    }

}
