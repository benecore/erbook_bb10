import bb.cascades 1.2

Container {
    id: root
    
    property alias text: textLabel.text
    property string imageSource: image.imageSource
    
    layout: DockLayout {
    }
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
 
 	signal clicked
 
    ImageButton {
        enabled: true
        defaultImageSource: "asset:///Images/Button/core_titlebar_button_inactive.amd"
        pressedImageSource: "asset:///Images/Button/core_titlebar_button_pressed.amd"
        disabledImageSource: "asset:///Images/Button/core_titlebar_button_disabled.amd"
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        onClicked: {
            root.clicked()
        }
    }   
    
    
    ImageView {
        id: image
        visible: (image.imageSource != "") && (textLabel.text == "")
        touchPropagationMode: TouchPropagationMode.None
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
    }
    
    
    Label {
        id: textLabel
        visible: (textLabel.text != "") && (image.imageSource == "")
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        touchPropagationMode: TouchPropagationMode.None
        textStyle{
            color: Settings.whiteHeader ? Color.Black : Color.White
        }
    }
    
    
}