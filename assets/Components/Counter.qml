import bb.cascades 1.2


Container {
    id: root
    layout: DockLayout{}
    
    property alias text: label.text
    
    background: back.imagePaint
    preferredWidth: 60
    preferredHeight: 60
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    attachedObjects: [
        ImagePaintDefinition {
            id: back
            imageSource: "asset:///Images/Counters/".concat(Qt.Settings ? Qt.Settings.activeColor : Settings.activeColor).concat(".png")
        }
    ]
    
    Label {
        id: label
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        textStyle.fontWeight: FontWeight.Bold
        textStyle.color: Color.White
        onTextChanged: {
            switch (text.length){
                case 1 || 2:
                    root.preferredWidth = 60
                    break;
                case 3:
                    root.preferredWidth = 70
                    break;
                case 4:
                    root.preferredWidth = 90
                    break;
                default:
                    break;
            }
        }
    }
    
}
