import bb.cascades 1.2
import bb.system 1.2


ListItemBase {
    id: root
    //maxWidth: (Qt.SizeHelper.maxWidth / 2) - 10
    contextActions: [
        ActionSet {
            id: actionSet
            DeleteActionItem {
                title: qsTr("Remove tag") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///Images/remove.png"
                onTriggered: {
                    if (Qt.Settings.isPro){
                        dialog.show()
                    }else{
                        Qt.Helper.showToast(qsTr("Can't remove tags in free version of app") + Retranslate.onLocaleOrLanguageChanged)
                    }
                }
                attachedObjects: [
                    SystemDialog {
                        id: dialog
                        title: qsTr("Remove '%1' tag").arg(ListItemData.text) + Retranslate.onLocaleOrLanguageChanged
                        body: qsTr("Are you sure you want to remove this tag?") + Retranslate.onLocaleOrLanguageChanged
                        confirmButton{
                            label: qsTr("Remove") + Retranslate.onLocaleOrLanguageChanged
                        }
                        onFinished: {
                            if (value == SystemUiResult.ConfirmButtonSelection){
                                Qt.App.indexPath = root.ListItem.indexPath
                                Qt.App.deleteTag(ListItemData.id)
                            }else{
                                console.log("CANCEL CLICKED")
                            }
                        }
                    }
                ]
            }
        }
    ]
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        leftPadding: 16
        rightPadding: 16
        topPadding: 16
        bottomPadding: 16
        topMargin: 0
        bottomMargin: 0
        
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            
            ImageView {
                verticalAlignment: VerticalAlignment.Center
                scalingMethod: ScalingMethod.AspectFit
                imageSource: Qt.App.whiteTheme ? "asset:///Images/tag_black.png" : "asset:///Images/tag_white.png"
            }
            
            Container {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                verticalAlignment: VerticalAlignment.Center
                
                Label {
                    topMargin: 1
                    text: ListItemData.text
                    textStyle.base: SystemDefaults.TextStyles.TitleText
                    multiline: true
                    autoSize.maxLineCount: 2
                    textStyle.fontWeight: FontWeight.W500
                    bottomMargin: 1
                }
            }
            
            Counter {
                minHeight: preferredHeight
                minWidth: preferredWidth
                maxHeight: preferredHeight
                maxWidth: preferredWidth
                verticalAlignment: VerticalAlignment.Center
                text: ListItemData.applied_count
                layoutProperties: StackLayoutProperties {
                    spaceQuota: -1
                }
            }
        }
    }
    
}