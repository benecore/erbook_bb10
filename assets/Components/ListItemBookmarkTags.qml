import bb.cascades 1.2
import bb.system 1.2


ListItemBase {
    id: root
    highlight: false
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        leftPadding: 16
        rightPadding: 16
        topPadding: 16
        bottomPadding: 16
        topMargin: 0
        bottomMargin: 0
        
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            
            ImageView {
                verticalAlignment: VerticalAlignment.Center
                scalingMethod: ScalingMethod.AspectFit
                imageSource: Qt.App.whiteTheme ? "asset:///Images/tag_black.png" : "asset:///Images/tag_white.png"
            }
            
            Container {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                verticalAlignment: VerticalAlignment.Center
                
                Label {
                    topMargin: 1
                    text: ListItemData.text
                    textStyle.base: SystemDefaults.TextStyles.TitleText
                    multiline: true
                    autoSize.maxLineCount: 2
                    textStyle.fontWeight: FontWeight.W500
                    bottomMargin: 1
                }
            }
            
            Container {
                background: Qt.App.whiteTheme ? Color.Black : Color.LightGray
                verticalAlignment: VerticalAlignment.Fill
                maxHeight: Infinity                        
                minWidth: 1
                maxWidth: 1
            }
            
            ImageView {
                verticalAlignment: VerticalAlignment.Center
                imageSource: Qt.App.whiteTheme ? "asset:///Images/remove_black.png" : "asset:///Images/remove.png"
                gestureHandlers: TapHandler {
                    onTapped: {
                        if (Qt.Settings.isPro){
                            dialog.show()
                        }else{
                            Qt.Helper.showToast(qsTr("Can't remove tags in free version of app") + Retranslate.onLocaleOrLanguageChanged)
                        }
                    }
                }
                attachedObjects: [
                    SystemDialog {
                        id: dialog
                        title: qsTr("Remove '%1' tag").arg(ListItemData.text) + Retranslate.onLocaleOrLanguageChanged
                        body: qsTr("Are you sure you want to remove this tag?") + Retranslate.onLocaleOrLanguageChanged
                        confirmButton{
                            label: qsTr("Remove") + Retranslate.onLocaleOrLanguageChanged
                        }
                        onFinished: {
                            if (value == SystemUiResult.ConfirmButtonSelection){
                                root.ListItem.view.removeBookmarkTag(root.ListItem.indexPath)
                            }else{
                                console.log("CANCEL CLICKED")
                            }
                        }
                    }
                ]
            }
        }
    }

}