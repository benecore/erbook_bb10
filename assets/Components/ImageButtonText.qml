import bb.cascades 1.2

Container {
    id: root
    
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    
    ImageButton {
        defaultImageSource: "asset:///Images/daymode.png"
        bottomMargin: 0
    }
    
    Label {
        topMargin: 0
        text: qsTr("Test")
        textStyle.textAlign: TextAlign.Center
    }
    
}