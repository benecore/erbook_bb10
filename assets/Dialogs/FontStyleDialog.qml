import bb.cascades 1.2

Container {
    id: fontSlider
    layout: DockLayout {}
    visible: false
    preferredHeight: 220
    horizontalAlignment: HorizontalAlignment.Fill
    background: back.imagePaint
    attachedObjects: [
        ImagePaintDefinition {
            id: back
            repeatPattern: RepeatPattern.X
            imageSource: "asset:///Images/patterns/262626.png"
        },
        FadeTransition {
            fromOpacity: 0
            toOpacity: 1
            duration: 200
        }
    ]
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Center
        
        Slider {
            topPadding: 5
            horizontalAlignment: HorizontalAlignment.Center
            fromValue: 32
            toValue: 64
            value: Settings.fontSize
            onImmediateValueChanged: {
                Settings.fontSize = (immediateValue)
                console.log("FONT SIZE: ".concat(immediateValue))
            }
        }
        
        Container {
            topPadding: 5
            leftPadding: 10
            rightPadding: 10
            horizontalAlignment: HorizontalAlignment.Fill
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            
            Button {
                enabled: (Settings.readFamily != 0)
                text: "Arial"
                onClicked: {
                    Settings.readFamily = 0
                    Helper.showToast(text)
                }
            }
            
            Button {
                enabled: (Settings.readFamily != 1)
                text: "Times New Roman"
                onClicked: {
                    Settings.readFamily = 1
                    Helper.showToast(text)
                }
            }
            
            Button {
                enabled: (Settings.readFamily != 2)
                text: "Comic Sans MS"
                onClicked: {
                    Settings.readFamily = 2
                    Helper.showToast(text)
                }
            }
        }
    }
}