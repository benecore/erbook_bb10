import bb.cascades 1.2

import "../Common"
import "../"
import "../Components"

NaviPage {
    id: root
    
    
    onPopTransitionEnded: {
        {page.destroy(); listView.clearSelection(); listViewGrid.clearSelection()}
        if (App.loading) App.loading = false
        page.active = false
    }
    
    attachedObjects: [
        ComponentDefinition {
            id: addPage
            source: "../AddPage.qml"
        },
        ComponentDefinition {
            id: readPage
            source: "../ReadPage.qml"
        }
    ]
    
    onCreationCompleted: {
        tabPane.naviPane = naviPane
    }
    
    MyPage{
        id: page
        
        
        function loadinglabel(current, all){
            page.loadingText = qsTr("Downloading content... %1/%2").arg(++current).arg(all)
        }
        
        
        function onTriggered(indexPath){
            var data = App.model.data(indexPath)
            App.indexPath = indexPath
            if (data){
                var page = readPage.createObject()
                page.article = data
                naviPane.push(page)
            }
        }
        
        
        onCreationCompleted: {
            App.currentSyncing.connect(loadinglabel)
        }
        
        header: TitleHeader {
            titleText: qsTr("Reading List") + Retranslate.onLocaleOrLanguageChanged
            counterVisible: (App.modelSize)
            counterText: App.modelSize
        }
        loading: App.loading
        noContentVisible: !App.modelSize && !App.loading && !App.syncing
        noContentText: qsTr("No articles") + Retranslate.onLocaleOrLanguageChanged
        noContentImageSource: App.whiteTheme ? "asset:///Images/readingList_black.png" : "asset:///Images/readingList.png"
        actionBarAutoHideBehavior: !SizeHelper.nType ? ActionBarAutoHideBehavior.Default : ActionBarAutoHideBehavior.HideOnScroll
        actions: [
            ActionItem {
                ActionBar.placement: ActionBarPlacement.OnBar
                title: qsTr("Add article") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///Images/addArticle.png"
                onTriggered: {
                    naviPane.push(addPage.createObject());
                }
            },
            ActionItem {
                ActionBar.placement: ActionBarPlacement.OnBar
                title: Settings.listLayout ? qsTr("GridView") + Retranslate.onLocaleOrLanguageChanged : qsTr("ListView") + Retranslate.onLocaleOrLanguageChanged
                imageSource: Settings.listLayout ? "asset:///Images/ic_view_grid.png" : "asset:///Images/ic_view_list.png"
                onTriggered: {
                    Settings.listLayout = !Settings.listLayout
                    if (Settings.listLayout){
                        toHide.target = listView
                        toHide.play()
                        toVisible.target = listView
                        toVisible.play()
                    }else{
                        toHide.target = listViewGrid
                        toHide.play()
                        toVisible.target = listViewGrid
                        toVisible.play()
                    }
                }
            },
            ActionItem {
                ActionBar.placement: ActionBarPlacement.OnBar
                title: qsTr("Sync") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///Images/reload.png"
                onTriggered: {
                    App.bookmarks(true)
                }
            }
        ]
        
        
        Container {
            layout: DockLayout {}
            
            attachedObjects: [
                ScaleTransition {
                    id: toVisible
                    toX: 1.0
                    toY: 1.0
                    duration: 350
                    easingCurve: StockCurve.CircularIn
                    fromX: 0.0
                    fromY: 0.0
                },
                ScaleTransition {
                    id: toHide
                    toX: 0.0
                    toY: 1.0
                    duration: 350
                    easingCurve: StockCurve.CircularIn
                    fromX: 1.0
                    fromY: 1.0
                }
            ]
            
            ListView {
                id: listView              
                leadingVisual: LeadingVisual {
                    
                }
                scrollRole: ScrollRole.Main
                opacity: App.loading || !Settings.listLayout || App.syncing ? 0 : 1
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                
                layout: StackListLayout {
                } 
                dataModel: App.model
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        ListItemArticles {}
                    }
                ]
                onTriggered: {
                    select(indexPath) 
                    page.onTriggered(indexPath)
                }
            } // End of ListView StackListLayout
            
            ListView {
                id: listViewGrid
                leadingVisual: LeadingVisual {
                
                }
                scrollRole: ScrollRole.Main
                opacity: App.loading || Settings.listLayout || App.syncing ? 0 : 1
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                
                layout: GridListLayout {
                    columnCount: 2
                } 
                dataModel: App.model
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        ListItemArticlesGrid {}
                    }
                ]
                onTriggered: {
                    select(indexPath) 
                    page.onTriggered(indexPath)
                }
            } // End of ListView StackListLayout
        } // End of DockLayout
    }

}
