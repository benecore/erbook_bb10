import bb.cascades 1.2
import bb.system 1.2
import com.devpda.tools 1.2

import "../Components"
import "../Common"

MyPage {
    id: root
    
    function userDone(user){
        user = user
        remoteImage.url = user.avatar_url
        fullnameLabel.text = user.first_name+" "+user.last_name
        tagsCount.text = user.tags.length
        publisherLabel.text = user.is_publisher ? qsTr("Yes") + Retranslate.onLocaleOrLanguageChanged 
        : qsTr("No") + Retranslate.onLocaleOrLanguageChanged
        usernameLabel.text = user.username
        publishMail.text = user.email_into_address
        kindleMail.text = user.kindle_mail_address
    }
    
    property alias title: titleHeader.titleText
    property alias subtitle: titleHeader.subtitleText
    property alias image: titleHeader.imageSource
    
    property variant user: null
    
    header: TitleHeader {
        id: titleHeader
        imageVisible: false
    }
    loading: false
    
    onCreationCompleted: {
        App.userInfoDone.connect(userDone)
        if (Cacher.getUserInfo() === undefined){
            App.userInfo()
        }else{
            console.debug("OFFLINE DATA")
            userDone(Cacher.getUserInfo())
        }
    }
    
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: qsTr("Refresh") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///Images/reload.png"
            onTriggered: {
                App.userInfo()
            }
        }
    ]
    
    
    attachedObjects: [
        Invoker {
            id: invoker
            onInvocationFailed: {
                console.debug("Invokation failed")
            }
        }
    ]
    
    
    ScrollView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        scrollViewProperties.pinchToZoomEnabled: false
        Container {
            
            Header {
                title: qsTr("Account info") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                leftPadding: 10
                rightPadding: 10
                topPadding: 10
                Container {
                    topPadding: 5
                    bottomPadding: 5
                    leftPadding: 5
                    rightPadding: 5
                    RemoteImage {
                        id: remoteImage
                        preferredHeight: 150
                        preferredWidth: 150
                        defaultImage: "asset:///Images/userAvatar.png"
                    }
                }
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    
                    Container {
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        
                        
                        Container {
                            topPadding: 5
                            bottomPadding: 5
                            leftPadding: 5
                            rightPadding: 5
                            minWidth: (SizeHelper.maxWidth - 170) / 3
                            maxWidth: (SizeHelper.maxWidth - 170) / 3
                            
                            Label {
                                textStyle.fontWeight: FontWeight.Bold
                                horizontalAlignment: HorizontalAlignment.Center
                                text: Cacher.getTotalArticles();
                            }
                            
                            Label {
                                text: qsTr("articles") + Retranslate.onLocaleOrLanguageChanged
                                horizontalAlignment: HorizontalAlignment.Center
                            }
                        }
                        
                        Container {
                            background: App.whiteTheme ? Color.Black : Color.LightGray
                            verticalAlignment: VerticalAlignment.Fill
                            horizontalAlignment: HorizontalAlignment.Left
                            maxHeight: Infinity                        
                            minWidth: 1
                            maxWidth: 1
                        }
                        
                        Container {
                            topPadding: 5
                            bottomPadding: 5
                            leftPadding: 5
                            rightPadding: 5
                            minWidth: (SizeHelper.maxWidth - 170) / 3
                            maxWidth: (SizeHelper.maxWidth - 170) / 3
                            
                            Label {
                                id: tagsCount
                                textStyle.fontWeight: FontWeight.Bold
                                horizontalAlignment: HorizontalAlignment.Center
                            }
                            
                            Label {
                                text: qsTr("tags") + Retranslate.onLocaleOrLanguageChanged
                                horizontalAlignment: HorizontalAlignment.Center
                            }
                        }
                        
                        Container {
                            background: App.whiteTheme ? Color.Black : Color.LightGray
                            verticalAlignment: VerticalAlignment.Fill
                            horizontalAlignment: HorizontalAlignment.Left
                            maxHeight: Infinity                        
                            minWidth: 1
                            maxWidth: 1
                        }
                        
                        Container {
                            topPadding: 5
                            bottomPadding: 5
                            leftPadding: 5
                            rightPadding: 5
                            minWidth: (SizeHelper.maxWidth - 170) / 3
                            maxWidth: (SizeHelper.maxWidth - 170) / 3
                            
                            Label {
                                id: publisherLabel
                                textStyle.fontWeight: FontWeight.Bold
                                horizontalAlignment: HorizontalAlignment.Center
                                textStyle{
                                    color: Color.create("#".concat(Settings.activeColor))
                                }
                            }
                            
                            Label {
                                text: qsTr("publisher") + Retranslate.onLocaleOrLanguageChanged
                                horizontalAlignment: HorizontalAlignment.Center
                            }
                        }
                    } // End of Photo/Articles/Tags container
                } 
            }   // End of account info
            
            Divider{}
            
            Container {
                leftPadding: 10
                rightPadding: 10
                
                Container {
                    visible: usernameLabel.text
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    bottomMargin: 5
                    Label {
                        text: qsTr("Logged as") + Retranslate.onLocaleOrLanguageChanged
                    }
                    
                    Label {
                        id: usernameLabel
                        textStyle.fontWeight: FontWeight.Bold
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        textStyle.textAlign: TextAlign.Right
                    }
                }
                
                Container {
                    visible: fullnameLabel.text
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    bottomMargin: 5
                    Label {
                        text: qsTr("Full name") + Retranslate.onLocaleOrLanguageChanged
                    }
                    
                    Label {
                        id: fullnameLabel
                        textStyle.fontWeight: FontWeight.Bold
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        textStyle.textAlign: TextAlign.Right
                    }
                }
                
                Container {
                    visible: kindleMail.text
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    bottomMargin: 5
                    Label {
                        text: qsTr("Kindle mail") + Retranslate.onLocaleOrLanguageChanged
                    }
                    
                    Label {
                        id: kindleMail
                        textStyle.fontWeight: FontWeight.Bold
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        textStyle.textAlign: TextAlign.Right
                    }
                }
                
                Container {
                    visible: publishMail.text
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    bottomMargin: 5
                    Label {
                        text: qsTr("Publish mail") + Retranslate.onLocaleOrLanguageChanged
                    }

                    Label {
                        id: publishMail
                        textStyle.fontWeight: FontWeight.Bold
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        textStyle.textAlign: TextAlign.Right
                        textFormat: TextFormat.Html
                        textStyle{
                            color: Color.create("#".concat(Settings.activeColor))
                        }
                        
                        gestureHandlers: [
                            TapHandler {
                                onTapped: {
                                    invoker.target = "sys.pim.uib.email.hybridcomposer"
                                    invoker.action = "bb.action.SENDEMAIL"
                                    invoker.invoke()
                                }
                            }
                        ]
                    }
                }
            } // End of detailed info
            
            Divider {}
            
            Button {
                horizontalAlignment: HorizontalAlignment.Center
                text: qsTr("Log out") + Retranslate.onLocaleOrLanguageChanged
                onClicked: {
                    signOut.show()
                }
                attachedObjects: [
                    SystemDialog {
                        id: signOut
                        title: qsTr("Log out") + Retranslate.onLocaleOrLanguageChanged
                        body: qsTr("Are you sure you want to log out?") + Retranslate.onLocaleOrLanguageChanged
                        confirmButton{
                            label: qsTr("Yes") + Retranslate.onLocaleOrLanguageChanged
                        }
                        onFinished: {
                            if (value == SystemUiResult.ConfirmButtonSelection){
                                Database.resetAuth()
                            }else{
                                console.log("CANCEL CLICKED")
                            }
                        }
                    }
                ]
            }
            
            Header {
                title: qsTr("Cache") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Button {
                horizontalAlignment: HorizontalAlignment.Center
                text: qsTr("Clear cache") + Retranslate.onLocaleOrLanguageChanged
                bottomMargin: 0
                onClicked: {
                    dialog.show()
                }
                attachedObjects: [
                    SystemDialog {
                        id: dialog
                        title: qsTr("Clear cache") + Retranslate.onLocaleOrLanguageChanged
                        body: qsTr("Are you sure you want to clear cache and all data assigned to articles?") + Retranslate.onLocaleOrLanguageChanged
                        confirmButton{
                            label: qsTr("Clear") + Retranslate.onLocaleOrLanguageChanged
                        }
                        onFinished: {
                            if (value == SystemUiResult.ConfirmButtonSelection){
                                if (Database.clearCache()){
                                    Helper.showToast(qsTr("Cache cleared") + Retranslate.onLocaleOrLanguageChanged)
                                    tabPane.refreshTab()
                                }else{
                                    Helper.showToast(qsTr("Unable to clear cache") + Retranslate.onLocaleOrLanguageChanged)
                                }
                            }else{
                                console.log("CANCEL CLICKED")
                            }
                        }
                    }
                ]
            }
            
            Label {
                topMargin: 0
                horizontalAlignment: HorizontalAlignment.Center
                text: qsTr("This removes all contents of articles") + Retranslate.onLocaleOrLanguageChanged
                textStyle.textAlign: TextAlign.Center
                textStyle.base: SystemDefaults.TextStyles.SubtitleText
            }
        }
    } // End of ScrollView
} // End of MyPage