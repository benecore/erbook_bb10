import bb.cascades 1.2

import "../Common"
import "../Components"



MyPage {
    id: root
    
    property string tagName
    
    header: TitleHeader {
        titleText: qsTr("Tag: %1").arg(tagName) + Retranslate.onLocaleOrLanguageChanged
        counterVisible: (App.modelSize)
        counterText: App.modelSize
    }
    loading: false
    
    attachedObjects: [
        ComponentDefinition {
            id: readPage
            source: "../ReadPage.qml"
        }
    ]
    
    
    function onTriggered(indexPath){
        var data = App.model.data(indexPath)
        App.indexPath = indexPath
        if (data){
            var page = readPage.createObject()
            page.article = data
            naviPane.push(page)
        }
    }
    actionBarAutoHideBehavior: !SizeHelper.nType ? ActionBarAutoHideBehavior.Default : ActionBarAutoHideBehavior.HideOnScroll
    
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: Settings.listLayout ? qsTr("GridView") + Retranslate.onLocaleOrLanguageChanged : qsTr("ListView") + Retranslate.onLocaleOrLanguageChanged
            imageSource: Settings.listLayout ? "asset:///Images/ic_view_grid.png" : "asset:///Images/ic_view_list.png"
            onTriggered: {
                Settings.listLayout = !Settings.listLayout
                if (Settings.listLayout){
                    toHide.target = listView
                    toHide.play()
                    toVisible.target = listView
                    toVisible.play()
                }else{
                    toHide.target = listViewGrid
                    toHide.play()
                    toVisible.target = listViewGrid
                    toVisible.play()
                }
            }
        }
    ]
    
    
    Container {
        layout: DockLayout {}
        
        attachedObjects: [
            ScaleTransition {
                id: toVisible
                toX: 1.0
                toY: 1.0
                duration: 350
                easingCurve: StockCurve.CircularIn
                fromX: 0.0
                fromY: 0.0
            },
            ScaleTransition {
                id: toHide
                toX: 0.0
                toY: 1.0
                duration: 350
                easingCurve: StockCurve.CircularIn
                fromX: 1.0
                fromY: 1.0
            }
        ]
        
        ListView {
            id: listView              
            leadingVisual: LeadingVisual {
            
            }
            scrollRole: ScrollRole.Main
            opacity: App.loading || !Settings.listLayout || App.syncing ? 0 : 1
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            layout: StackListLayout {
            } 
            dataModel: App.model
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    ListItemArticles {}
                }
            ]
            onTriggered: {
                root.onTriggered(indexPath)
            }
        } // End of ListView StackListLayout
        
        ListView {
            id: listViewGrid
            leadingVisual: LeadingVisual {
            
            }
            scrollRole: ScrollRole.Main
            opacity: App.loading || Settings.listLayout || App.syncing ? 0 : 1
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            layout: GridListLayout {
                columnCount: 2
            } 
            dataModel: App.model
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    ListItemArticlesGrid {}
                }
            ]
            onTriggered: {
                root.onTriggered(indexPath)
            }
        } // End of ListView StackListLayout
    } // End of DockLayout
}