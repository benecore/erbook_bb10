import bb.cascades 1.2

import "../Common"
import "../Components"



MyPage {
    id: root
    
    
    function  loadTags(listTags){
        groupModel.insertList(listTags)
        App.loading = false
    }
    
    
    function tagDeleted(){
        Cacher.removeTag(tagId)
        Cacher.removeBookmarkTag(bookmarkId, tagId);
        groupModel.removeAt(indexPath)
        tabPane.refreshTab();
        Helper.showToast(qsTr("Tag removed from bookmark") + Retranslate.onLocaleOrLanguageChanged)
        
    }
    
    
    property string bookmarkId: ""
    property string tagId: ""
    property variant indexPath
    
    header: TitleHeader {
        titleText: qsTr("Bookmark tags") + Retranslate.onLocaleOrLanguageChanged
    }
    loading: App.loading
    loadingText: qsTr("Loading bookmark tags...") + Retranslate.onLocaleOrLanguageChanged
    actionBarAutoHideBehavior: !SizeHelper.nType ? ActionBarAutoHideBehavior.Default : ActionBarAutoHideBehavior.HideOnScroll
    
    
    onCreationCompleted: {
        App.bookmarkTagDeleted.connect(tagDeleted)
        App.loading = true
    }
    
    onActiveChanged: {
        if (active){
            loadTags(Cacher.getBookmarkTags(bookmarkId))
        }else{
            if (App.loading) App.loading = false
        }
    }
    
    
    ListView {
        id: listView
        
        function removeBookmarkTag(indexPath){
            root.indexPath = indexPath
            var data = dataModel.data(indexPath)
            if (data){
                root.tagId = data.id
                App.deleteBookmarkTag(root.bookmarkId,root.tagId)
            }
        }
        
        scrollRole: ScrollRole.Main
        
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        layout: StackListLayout {
            headerMode: ListHeaderMode.Sticky
        } 
        dataModel: GroupDataModel {
            id: groupModel
            grouping: ItemGrouping.None
            sortedAscending: false
            sortingKeys: ["text"]
        }
        listItemComponents: [
            ListItemComponent {
                type: "item"
                ListItemBookmarkTags {}
            }
        ]
    } // End of ListView StackListLayout

}