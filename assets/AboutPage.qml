import bb.cascades 1.2
import bb 1.0
import com.devpda.tools 1.2
import bb.platform 1.2

import "Common"
import "Components"

MyPage {
    
    header: TitleHeader {
        titleText: qsTr("About") + Retranslate.onLocaleOrLanguageChanged
    }
    loading: false
    property bool payWorking: false
    onActiveChanged: {
        if (active)
            aboutAction.enabled = false
        else
            aboutAction.enabled = true
    }
    
    attachedObjects: [
        ApplicationInfo {
            id: appInfo  
        },
        PaymentManager {
            id: payManager
            onPurchaseFinished: {
                if (reply.errorCode == 0){
                    Helper.showToast(qsTr("Thank you for your support. Now you have a PRO version. Enjoy ;)") + Retranslate.onLocaleOrLanguageChanged)
                    Settings.isPro = true
                }else{
                    //Helper.showToast(reply.errorText)
                    console.log("ERROR PURCHASE ITEM: ".concat(reply.errorText))
                }
                payWorking = false
            }
            onExistingPurchasesFinished: {
                if (reply.errorCode == 0) {
                    if (reply.purchases.length == 0){
                        Helper.showToast(qsTr("No existing purchased") + Retranslate.onLocaleOrLanguageChanged)
                        Settings.isPro = false;
                    }else{
                        Helper.showToast(qsTr("Purchase restored. Thank you") + Retranslate.onLocaleOrLanguageChanged)
                        Settings.isPro = true
                    }
                }else{
                    console.log("ERROR EXISTING PURCHASED: ".concat(reply.errorText))
                }
                payWorking = false
            }
        }
    ]
    
    
    ScrollView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            layout: DockLayout {}
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            ImageView {
                horizontalAlignment: HorizontalAlignment.Right
                verticalAlignment: VerticalAlignment.Bottom
                imageSource: "asset:///Images/devpda.png"
                opacity: 0.5
            }
            
            Container {
                topPadding: 15
                leftPadding: 15
                rightPadding: 15
                bottomPadding: 15
                
                Label {
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("eRBook %1").arg(Settings.isPro ? "PRO" : "FREE") + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        base: SystemDefaults.TextStyles.BigText
                        fontWeight: FontWeight.Bold
                    }
                    bottomMargin: 0
                }
                
                Label {
                    topMargin: 0
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Version: %1").arg(appInfo.version) + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        base: SystemDefaults.TextStyles.TitleText
                    }
                    bottomMargin: 0
                }
                
                Label {
                    topMargin: 0
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Developer: %1").arg("Zoltán Benke") + Retranslate.onLocaleOrLanguageChanged
                    textStyle{
                        base: SystemDefaults.TextStyles.PrimaryText
                    }
                }
                
                Divider {
                
                }
                
                Label {
                    text: qsTr("A fully featured Readability client that allows you to take your reading list.") + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                    textStyle.fontStyle: FontStyle.Italic
                    textStyle.base: SystemDefaults.TextStyles.TitleText
                }
                
                Label {
                    text: qsTr("In case of any questions or ideas, please contact me:") + Retranslate.onLocaleOrLanguageChanged
                    multiline: true
                    textStyle.fontStyle: FontStyle.Italic
                    textFormat: TextFormat.Html
                }
                
                Button {
                    horizontalAlignment: HorizontalAlignment.Center
                    imageSource: App.whiteTheme ? "asset:///Images/contact_me_black.png" : "asset:///Images/contact_me.png"
                    onClicked: {
                        invoker.target = "sys.pim.uib.email.hybridcomposer"
                        invoker.action = "bb.action.SENDEMAIL"
                        invoker.uri = "mailto:support@devpda.net?subject=eRBook - Support"
                        invoker.invoke()
                    }
                    attachedObjects: [
                        Invoker {
                            id: invoker
                        }
                    ]
                }
                
                Divider {
                    bottomMargin: 0
                }
                
                Label {
                    topMargin: 0
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Copyright (c) 2013 DevPDA") + Retranslate.onLocaleOrLanguageChanged
                    textStyle.color: Color.create("#".concat(Settings.activeColor))
                    textStyle.base: SystemDefaults.TextStyles.PrimaryText
                    textStyle.fontWeight: FontWeight.Bold
                }
                
                
                Divider {
                    visible: !Settings.isPro
                }
                
                Label {
                    visible: !Settings.isPro
                    horizontalAlignment: HorizontalAlignment.Center
                    multiline: true
                    textStyle.textAlign: TextAlign.Center
                    text: qsTr("This is a FREE version of eRBook. Free version has a three limitations\n\n<i>- Can't add/remove tags of articles</i>\n<i>- Can't change Style of reading</i>\n<i>- Can't change font</i>") + Retranslate.onLocaleOrLanguageChanged
                    textStyle.fontWeight: FontWeight.Bold
                    textFormat: TextFormat.Html
                }
                
                Container {
                    visible: !Settings.isPro
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Fill
                    Button {
                        enabled: !payWorking
                        text: qsTr("Buy PRO version") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            payWorking = true
                            payManager.requestPurchase("","eRBook-PRO","eRBook PRO version")
                        }
                    }
                    
                    Button {
                        enabled: !payWorking
                        text: qsTr("Already purchased") + Retranslate.onLocaleOrLanguageChanged
                        onClicked: {
                            payWorking = true
                            payManager.requestExistingPurchases(true)
                        }
                    }
                }
            
            } // TopToBottom container
        }
    }
}