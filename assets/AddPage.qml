import bb.cascades 1.2
import bb.system 1.2

import "Common"
import "Components"

MyPage {
    id: root
    
    function addBookmark(){
        if (!textField.validator.valid){
            Helper.showToast(qsTr("You must enter a valid url") + Retranslate.onLocaleOrLanguageChanged)
            return;
        }
        App.addBookmark(textField.text, favoriteBtn.status , archiveBtn.status, allowBtn.status)
    }
    
    function numericOnly(textin) {
        
        var m_strOut = new String (textin);
        m_strOut = m_strOut.replace(/[^\d.]/g,'');
        
        return m_strOut;
    }
    
    header: TitleHeader {
        titleText: qsTr("Add article") + Retranslate.onLocaleOrLanguageChanged
    }
    loading: App.loading
    loadingText: qsTr("Adding article...") + Retranslate.onLocaleOrLanguageChanged
    onActiveChanged: {
        if (!active)
        	textField.text = ""
        else
        	textField.requestFocus()
    }
    
    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            title: qsTr("Back") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                naviPane.pop()
            }
        }
    }

    
    Container {
        layout: DockLayout {}
        enabled: !App.loading
        opacity: App.loading ? 0.3 : 1
        ScrollView {
            
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            Container {
                topPadding: 15
                leftPadding: 15
                rightPadding: 15
                
                
                TextField {
                    id: textField
                    hintText: qsTr("url") + Retranslate.onLocaleOrLanguageChanged
                    inputMode: TextFieldInputMode.Url
                    maximumLength: 300
                    input.submitKey: SubmitKey.Submit
                    input.onSubmitted: {
                        console.debug("ADD ARTICLE: ARCHIVE: ".concat(archiveBtn.status).concat(" FAVORITE: ").concat(favoriteBtn.status).concat(" ALLOW: ").concat(allowBtn.status))
                        addBookmark()
                    }
                    validator: Validator {
                        mode: ValidationMode.Immediate
                        errorMessage: qsTr("Invalid URL") + Retranslate.onLocaleOrLanguageChanged
                        onValidate: {
                            if (Helper.validUrl(textField.text)){
                                valid = true
                            }else{
                                valid = false
                            }
                        }
                    }
                }
                
                Divider {}
                
                Container {
                    layout: DockLayout{}
                    
                    horizontalAlignment: HorizontalAlignment.Fill
                    topPadding: 5
                    
                    Label{
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Archive") + Retranslate.onLocaleOrLanguageChanged
                    }
                    
                    ToggleButton {
                        id: archiveBtn
                        
                        property int status: 0
                        
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                        onCheckedChanged: {
                            if (checked){
                                status = 1
                            }else{
                                status = 0
                            }
                        }
                    }
                }
                
                Container {
                    layout: DockLayout{}
                    
                    horizontalAlignment: HorizontalAlignment.Fill
                    topPadding: 5
                    
                    Label{
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Favorite") + Retranslate.onLocaleOrLanguageChanged
                    }
                    
                    ToggleButton {
                        id: favoriteBtn
                        
                        property int status: 0
                        
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                        onCheckedChanged: {
                            if (checked){
                                status = 1
                            }else{
                                status = 0
                            }
                        }
                    }
                }
                
                Container {
                    layout: DockLayout{}
                    
                    horizontalAlignment: HorizontalAlignment.Fill
                    topPadding: 5
                    
                    Label{
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Allow duplicates *") + Retranslate.onLocaleOrLanguageChanged
                    }
                    
                    ToggleButton {
                        id: allowBtn
                        
                        property int status: 0
                        
                        horizontalAlignment: HorizontalAlignment.Right
                        verticalAlignment: VerticalAlignment.Center
                        onCheckedChanged: {
                            if (checked){
                                status = 1
                            }else{
                                status = 0
                            }
                        }
                    
                    }
                }
                
                Label {
                    multiline: true
                    autoSize.maxLineCount: 2
                    text: qsTr("* Allow duplicates automatically rewrite existing article") + Retranslate.onLocaleOrLanguageChanged
                    textStyle.base: SystemDefaults.TextStyles.SubtitleText
                }
                
                
                Button {
                    horizontalAlignment: HorizontalAlignment.Center
                    text: qsTr("Add article") + Retranslate.onLocaleOrLanguageChanged
                    onClicked: {
                        console.debug("ADD ARTICLE: ARCHIVE: ".concat(archiveBtn.status).concat(" FAVORITE: ").concat(favoriteBtn.status).concat(" ALLOW: ").concat(allowBtn.status))
                        addBookmark()
                    }
                }
            }
        }
    }
}