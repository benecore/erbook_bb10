import bb.cascades 1.2
import com.devpda.tools 1.2

import "../Common"
import "../Components"


MyPage {
    id: root
    
    function setMessage(message){
        result.text = message
    }        
    
    
    function requestChanged(){
        timer.start()
        result.text = ""
    }
    
    header: TitleHeader {
        titleText: qsTr("Sharing article")
        leftButtonText: qsTr("Close") + Retranslate.onLocaleOrLanguageChanged
        onLeftButtonClicked: {
            App.cardDone("Card closed")
            if (Settings.closeInvoke){
                Application.requestExit()
            }
        }
    }
    loading: false
    
    onCreationCompleted:{
        App.invokationMessage.connect(setMessage)
        App.requestChanged.connect(requestChanged)
        timer.start()
    }
    
    attachedObjects: [
        Timer {
            id: timer
            interval: 700
            onTimeout: {
                App.addBookmark(App.uri,0,0,0)
                stop()
            }
        }
    ]
    
    Container {
        layout: DockLayout {}
        leftPadding: 15
        rightPadding: 15
        topPadding: 5
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            layout: StackLayout {
            }
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Top
            TextField {
                id: textField
                enabled: false
                hintText: qsTr("url") + Retranslate.onLocaleOrLanguageChanged
                inputMode: TextFieldInputMode.Url
                maximumLength: 300
                preferredHeight: 300
                text: App.uri
            }
            
            Label {
                id: result
                visible: (text != "")
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.textAlign: TextAlign.Center
                textStyle{
                    base: SystemDefaults.TextStyles.BodyText
                }
            }
        }
        ActivityIndicator {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center  
            preferredHeight: 300
            preferredWidth: 300
            visible: App.loading
            running: visible
        }
    }
}