import bb.cascades 1.2

import "Common"
import "Components"

Sheet {
    id: root
    
    
    peekEnabled: false
    property url authUrl
    
    
    function setUrl(authorizationUrl){
        authUrl = authorizationUrl
        console.log("AUTH URL DONE: ".concat(authUrl))
        webView.url = authUrl
    }
    
    
    onOpened: {
        Api.authorizationUrlReceived.connect(setUrl)
        Database.authorizedChanged.connect(close)
        webView.storage.clear();
        App.loading = true
        Api.setCallbackUrl("http://devpda.net/oauth.php");
        Api.setToken("")
        Api.setTokenSecret("")
        Api.getRequestToken();
    }
    
    onClosed: {
        destroy()
    }
    
    
    MyPage {
        
        
        header: TitleHeader {
            titleText: qsTr("Web login") + Retranslate.onLocaleOrLanguageChanged
            leftButtonText: webView.canGoBack ? qsTr("Back") + Retranslate.onLocaleOrLanguageChanged : qsTr("Close") + Retranslate.onLocaleOrLanguageChanged
            onLeftButtonClicked: {
                if (App.loading || !webView.canGoBack){
                    App.loading = false
                    close()
                }
                if (webView.canGoBack){
                    webView.goBack()
                }
            }
            rightButtonImageSource: "asset:///Images/reload.png"
            onRightButtonClicked: {
                webView.url = authUrl
                //webView.reload()
            }
        }
        loading: false
        
        Container {
            layout: DockLayout{}
            
            ScrollView {
                id: scrollView
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                scrollViewProperties {
                    scrollMode: ScrollMode.Both
                    pinchToZoomEnabled: true
                }
                
                WebView {
                    id: webView
                    
                    onNavigationRequested: {
                        console.log("NavigationRequested: " + request.url + " navigationType=" + request.navigationType)
                        if (request.url.toString().indexOf("oauth_verifier") != -1){
                            App.accessToken(request.url)
                            App.loading = true
                        }else{
                            console.log("NEOBSAHUJE")
                        }
                    }
                    
                    settings.viewport: {
                        "width": "device-width",
                        "initial-scale": 1.0
                    }
                    
                    
                    onLoadProgressChanged: {
                        // Update the ProgressBar while loading.
                        progressIndicator.value = loadProgress / 100.0
                    }
                    
                    
                    onMinContentScaleChanged: {
                        // Update the scroll view properties to match the content scale
                        // given by the WebView.
                        scrollView.scrollViewProperties.minContentScale = minContentScale;
                        
                        // Let's show the entire page to start with.
                        scrollView.zoomToPoint(0,0, minContentScale,ScrollAnimation.None)
                    }
                    
                    onMaxContentScaleChanged: {
                        // Update the scroll view properties to match the content scale 
                        // given by the WebView.
                        scrollView.scrollViewProperties.maxContentScale = maxContentScale;
                    }
                    
                    onLoadingChanged: {
                        if (loadRequest.status == WebLoadStatus.Started) {
                            // Show the ProgressBar when loading started.
                            App.loading = false
                            console.log("WebView Started")
                            progressIndicator.opacity = 1.0
                        } else if (loadRequest.status == WebLoadStatus.Succeeded) {
                            // Hide the ProgressBar when loading is complete.
                            console.log("WebView Succeeded")
                            progressIndicator.opacity = 0.0
                        } else if (loadRequest.status == WebLoadStatus.Failed) {
                            // If loading failed, fallback a local html file which will also send a Java script message                        
                            console.log("WebView failed")
                            
                            progressIndicator.opacity = 0.0
                        }
                    }
                    settings.imageDownloadingEnabled: false
                }
            } // End of scrollview
            
            
            ActivityIndicator {
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                preferredHeight: 300
                preferredWidth: 300
                visible: App.loading
                running: visible
            }
            
            Container {
                bottomPadding: 25
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Bottom
                
                ProgressIndicator {
                    id: progressIndicator
                    opacity: 0
                }
            }
        }
    }
}