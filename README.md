## eRBook - Readability.com client ##

**eRBook** is a fully-featured Readability.com client for `BlackBerry` Z10/Q10/Q5

Release notes:
-
### v1.1.1.6 (`30.3.2014`)
- Minor changes
- Stability and performance improvements
### v1.1.1.4 (`20.2.2014`)
**New**

- Added Tags support 
- Added new color schemes 
- New app settings 
- Improved style of reading articles 
- Improved List/Grid styles 
- Improved sharing from external apps 
- Changed FREE version limitations. Now is possible to Add articles from free version

**Fixed**

- Splash screens  
- Sharing options from context menu 
- Login issue = Added support for web login 
- Czech translation for share articles 
- Many bugs fixes and performance improvements
- and more...
### v1.1.0.8 (`01.2.2014`)
- Improved existing reading styles
- Added Sepia (paperback) style
- Added three fonts (Arial, Times New Roman, Comic Sans MS)
- Added Style switcher in reading page
- Added Readability url shortener service (RDD.ME)
- Minor fixes
### v1.1.0.7 (`26.1.2014`)
- Minor update
- Improved UI 
- Improved article reading page 
- Night/Day mode 
- Share menu integration (ability to add new article from system share menu)
- Ability to change font size in reading page
- Color schemes of applications
- Minor changes 
- Many bugs fixes
### v1.0.0.3 (`11.9.2013`)
* Fixed cover creation
* Updated translations
* Minor fixes
### v1.0.0.2 (`10.9.2013`)
- Added Donations support
- Added URL validation (add page)
- Fixed cover creation
- Fixed change article when app is offline
- Minor fixes
### v1.0.0.1 (`4.9.2013`)
- Added Czech language
- Ability to Invite a Friend to Download app (BBM)
- Small fixes
### v1.0.0.0 (`4.9.2013`)
- Initial version